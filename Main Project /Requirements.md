Here is the initial requirements that should be met at your practice at Stone as network engineer:
--------------------------------------------------------------------------------------------------

1.  Deployment of lab environment, consisting 3x vSRX and 3x hosts/clients – Stone Computers cloud resources will be provided
2.  Initial configurations of devices deployed;
3.  Clustering of 2x vSRX (enabling reth interfaces and deploying A-A cluster);
4.  Prepare and deploy scenarios of one to fail, second to take traffic and restore;
5.  Validate configs
6.  Deploying Juniper JSA at lab environment;
7.  Exporting flow traffic and logs from firewalls;
8.  Prepare and present at least 3 scenarios of event correlation and log analytics