**Introduction**
================

The purpose of the risk assessment was to identify threats and vulnerabilities
related to the project.
The risk assessment will be utilized to identify risk mitigation plans related
to the project. The project was identified as a potential high-risk system in
the Department’s annual enterprise risk assessment


**Risk Assessment Approach**
============================

Participants:

| **Participant**    | **Role**        |
|--------------------|-----------------|
| Aleksandra Slavova | Team Leader     |
| Supervisor         | Company guy     |


**Risk MODEL**

In determining risk associated with the project, we utilized the following model
for classifying risk:

Risk = Threat likelihood x Magnitude of the Impact
--------------------------------------------------


**Threat Likelihood**

| **Likelihood** | **Definition**                                                                                                                                                   |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| High           | The threat-source is highly motivated and sufficiently capable, and controls to prevent the vulnerability from being exercised are ineffective                   |
| Medium         | The threat-source is motivated and capable, but controls are in place that may impede successful exercise of the vulnerability.                                  |
| Low            | The threat-source lacks motivation or capability, or controls are in place to prevent, or at least significantly impede, the vulnerability from being exercised. |


**Magnitude of Impact**

| **Impact**      | **Definition**                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **High (100)**  | The loss of confidentiality, integrity, or availability could be expected to have a severe or catastrophic adverse effect on organizational operations, organizational assets, or individuals. Examples: • A severe degradation in or loss of mission capability to an extent and duration that the organization is not able to perform one or more of its primary functions • Major damage to organizational assets • Major financial loss                                      |
| **Medium (50)** | The loss of confidentiality, integrity, or availability could be expected to have a serious adverse effect on organizational operations, organizational assets, or individuals. Examples: • Significant damage to organizational assets • Significant financial loss • Significant harm to individuals that does not involve loss of life or serious life-threatening injuries.                                                                                                  |
| **Low (10)**    | The loss of confidentiality, integrity, or availability could be expected to have a limited adverse effect on organizational operations, organizational assets, or individuals. Examples: • Degradation in mission capability to an extent and duration that the organization is able to perform its primary functions, but the effectiveness of the functions is noticeably reduced • Minor damage to organizational assets • Minor financial loss • Minor harm to individuals. |


**Risks that might occur**

1.  The server room is a fire hazard place and if something is to happen, work
    on the project might be interrupted.

2.  Password strength - Passwords used by the web application are
    inappropriately formulated. Attackers could guess the password of a user to
    gain access to the system.

3.  Disaster recovery - There are no procedures to ensure the ongoing operation
    of the system in event of a significant business interruption or disaster.

4.  Lack of documentation - System specifications, design and operating
    processes are not documented.
  


**Risk Assessment Results**

| \# | Observation                                                                                                   | Threat-source                       | Existing controls                                        | Likelihood | Impact | Risk Rating | Recommended controls                                                                                                                                                                                                                     |
|----|---------------------------------------------------------------------------------------------------------------|-------------------------------------|----------------------------------------------------------|------------|--------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | Fire in the server room                                                                                       | Fire                                | Smokes detectors and water sprinklers                    | Low        | High   | High        | Room should be cooled with air conditioner for maintaining low temperatures on devices.                                                                                                                                                  |
| 2  | User system passwords can be guessed or cracked                                                               | Hackers / Password effectiveness    | Passwords must be alphanumeric and at least 5 characters | Medium     | Medium | Medium      | Use of SSH keys instead of password.                                                                                                                                                                                                     |
| 3  | Disaster recovery plan has not been established                                                               | Environment / Disaster Recovery     | Weekly backup only                                       | Medium     | High   | Medium      | Develop and test a disaster recovery plan                                                                                                                                                                                                |
| 4  | Lack of documentation                                                                                         |                                     | Writing everything down                                  | Low        | High   | Medium      | Re-check all documents to confirm all are up-to-date.                                                                                                                                                                                    |
