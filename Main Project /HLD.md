<h1>High Level Design</h1>
<h4>Introduction</h4>
<p>This document will serve as a reference guide throughout the project.
In this High-Level Design, an overview for the designed network will be presented and additional
technical details will be provided.
The purpose of this High-Level Design (HLD) Document is to add the necessary details to the
current project description.
The goal of the HLD documentation is to present the structure of the system and the different
aspects of the network section itself.
The HLD uses mildly-technical to high-technical terms which should be understandable to the
audience.
The audience for the HLD includes the contributors to the project itself, the lecturers for the
appropriate classes and the Company.
Additional explanations will be provided per footnotes throughout the document.
Further details concerning the project itself can be found in the Project Plan document.</p>
<h4>Overview</h4>
<p>The following diagram gives an overview of the network topology, that the team will be using in
this project.</p>

![HLD_diagram](Main Project /Untitled_Diagram__1_.png)

<br><p>I will use 3 x vSRXs and 3 x Debian Hosts provided by the Company. There will be OSPF running between the devices as well as Chassis Cluster between vSRX1 and vSRX2 and JSA
Secure Analitycs on my Host3. The vSRX1 will be the primary node in my Chassis Cluster and the vSRX2 is the secondary.</p>
<h4>Performance</h4>
<p>Performance will not be an issue because this network is not going to be used in production. This
network is going to be set up for learning and experimentation. The network is going to run
virtually on a local machine, so the performance is not expected to be an issue.
When the network is up, an attempt will be made to achieve an uptime of around 99,999% with
minimal performance for testing and experimenting with the various protocols.</p>
<h4>Security</h4>
<p>     </p>
<h4>Hardware/Software</h4>
<p>     </p>
<h4>Protocols and standards???</h4>
<ul>
    <li>OSPFv3</li>
    <li>IPv6</li>
    <li>EUI-64</li>
    <li>VLAN</li>
    <li>HTTP</li>
    <li>TCP</li>
    <li>UDP</li>
    <li>ICMP</li>
    <li>SSH</li>
    <li>VPN</li>
</ul>
<h4>IP layout</h4>

|  **Device**         |  **VLAN**    |  **IP address**     |  **Subnet**  |
|--------------       |--------------|-------------------  |--------------|
|vSRX1 - vSRX2 - VSRX3|    VLAN101   | 2001:0:1:2:EUI-64   |    /64       |
| vSRX1 - PC1         |    VLAN102   | 2001:0:1:3:EUI-64   |    /64       |
| vSRX2 - PC2         |    VLAN103   | 2001:0:1:7:EUI-64   |    /64       |
| vSRX3 - PC3         |    VLAN104   | 2001:0:1:10:EUI-64  |    /64       |
| Firewall - PE       |    VLAN105   | 2001:0:1:12:EUI-64  |    /64       |  



                       
                       

<h4>Naming convention</h4>
<ul>
    <li>We have 3 different areas caled Copenhagen, London and Paris.</li>
    <li>Copenhagen - The router in Copenhagen area.</li>
    <li>London - The router in London area.</li>
    <li>Paris - The router in Paris area.</li>
    <li>DMZ - The accessible zone in our network.</li>
    <li>Workstation and Webservers - We have a workstation and a web server in every area and one workstation in our DMZ netwrok.</li>
    <li>ESXi Terminal - The terminal for the ESXi.</li>
    <li>Jump host - The Jump host in the MDGT Network</li>
    <li>DNS - The DNS server in Paris area.</li>
    <li>LibreNMS - The LibreNMS server in Copenhagen area.</li>
    <li>SRX Firewall - the router that has our firewall</li>
    <li>PE router - this is the router connectet to the backbone area.</li>
    <li>NIC1 and NIC0 - our ports on the blade.</li>
<ul>